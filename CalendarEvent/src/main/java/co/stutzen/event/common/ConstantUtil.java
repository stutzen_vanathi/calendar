package co.stutzen.event.common;

public class ConstantUtil {

	public static final int DAY_IN_SECONDS = 24 * 60 * 60;
	public static final int DAY_IN_MILLISECONDS = 24 * 60 * 60 * 1000;
	public static final int WEEK_IN_SECONDS = 7 * 24 * 60 * 60;
	public static final int LEAP_YEAR_IN_SECONDS = 31622400;
	public static final int NORMAL_YEAR_IN_SECONDS = 31536000;
	public static final int DAYS = 7;
	public static final int LEAP_YEAR = 4;
	public static final int MILLISECONDS_TO_SECONDS = 1000;
	public static final String CREATED = "created";
	public static final String DELETE_ALL = "all";
	public static final String DELETE_SINGLE = "single";
	public static final String DELETE_FOLLOWING_EVENT = "followingevent";
	public static final String DELETE_ALL_COMMENT = "All event has been removed";
	public static final String DELETE_SINGLE_COMMENT = "Single event has been removed";
	public static final String DELETE_FOLLOWING_EVENT_COMMENT = "Following event has been removed";
	public static final String COMPLETED_COMMENT = "Task has been completed";
	public static final String COMPLETED = "completed";
	public static final String REMOVED = "Removed";
	public static final String UPDATE_ALL = "all";
	public static final String UPDATE_SINGLE = "single";
	public static final String UPDATE_FOLLOWING_EVENT = "followingevent";
	public static final String NO_STATUS = "";
	public static final String WEEKLY = "weekly";
	public static final String MONTHLY = "monthly";
	public static final String YEARLY = "yearly";
	public static final String DAILY = "daily";
	public static final int EVENT_TYPE = 1;
	public static final int TASK_TYPE = 2;
	public static final String NO_REPEAT_TYPE = "NO";
	public static final Boolean IS_SINGLE = false;
	public static final byte IS_ACTIVE = 0;
	public static final String EVENT = "event";
	public static final String TASK = "task";
	public static int EVENT_ID = 0;
	public static String SINGLE_EVENT = "single";
	public static String ALL = "1";
	public static String FOLLOWING_EVENT = "2";
	public static String SINGLE = "3";
}
