package co.stutzen.event.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Test {

	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	try {
			Date date1 = sdf.parse("2015-01-01 12:00:00");
			Date date2 = sdf.parse("2015-02-01 00:00:00");
			Calendar dateTime1 = Calendar.getInstance();
			dateTime1.setTime(date1);
			Calendar dateWithoutTime1 = Calendar.getInstance();
			int year = dateTime1.get(Calendar.YEAR);
			int month = dateTime1.get(Calendar.MONTH);
			int day = dateTime1.get(Calendar.DAY_OF_MONTH);
			dateWithoutTime1.set(Calendar.YEAR, year);
			dateWithoutTime1.set(Calendar.MONTH, month);
			dateWithoutTime1.set(Calendar.DAY_OF_MONTH, day);
			dateWithoutTime1.set(Calendar.HOUR_OF_DAY, 0);
			dateWithoutTime1.set(Calendar.MINUTE, 0);
			dateWithoutTime1.set(Calendar.SECOND, 0);
			System.out.println(dateWithoutTime1.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	

	}

}
