package co.stutzen.event.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateFormatUtil {

	static SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
	
	SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	
	private static SimpleDateFormat sqlDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public static Date getDateTimeFormat(String dateTime)
			throws java.text.ParseException {
		Date dateTimeFormat = sqlDateTimeFormat.parse(dateTime);
		return dateTimeFormat;
	}

	public static Date getDateObject(String date) {
		Date d = null;
		try {
			d = f.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return d;
	}

	public static Date addYear(Date startDate, int yearCount) {
		Calendar c = Calendar.getInstance();
		c.setTime(startDate);
		c.add(Calendar.YEAR, yearCount);
		return c.getTime();
	}

	public static Date addSeconds(Date startDate, long intervalInSec) {
		Long days = (intervalInSec / ConstantUtil.DAY_IN_SECONDS);
		Calendar c = Calendar.getInstance();
		c.setTime(startDate);
		c.add(Calendar.DATE, days.intValue());
		return c.getTime();
	}

	public static Date findMonth(Date startDate, int monthCount, int day) {
		Calendar c = Calendar.getInstance();
		c.setTime(startDate);
		c.add(Calendar.MONTH, monthCount);
		Date d = getDayOfTheMonth(c.getTime(), day);
		return d;
	}

	public static Date addMonth(Date startDate, int monthCount) {
		Calendar c = Calendar.getInstance();
		c.setTime(startDate);
		c.add(Calendar.MONTH, monthCount);
		return c.getTime();
	}

	public static Date addDay(Date startDate, int dayCount) {
		Calendar c = Calendar.getInstance();
		c.setTime(startDate);
		c.add(Calendar.DATE, dayCount);
		return c.getTime();
	}

	public static Date getDayOfTheMonth(Date givenDate, int day) {
		Calendar c = Calendar.getInstance();
		c.setTime(givenDate);
		String formedDate = c.get(Calendar.YEAR) + "-"+ (c.get(Calendar.MONTH) + 1) + "-" + day;
		Date formedDateObj = getDateObject(formedDate);
		return formedDateObj;
	}

	public static int getDayOfTheMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		return dayOfMonth;
	}

	public static int getDayOfTheWeek(Date eventStart) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(eventStart);
		int currentDay = cal.get(Calendar.DAY_OF_WEEK);
		return currentDay;
	}

	public static long dayDiff(Date startDate, Date endDate) {
		Calendar c = Calendar.getInstance();
		c.setTime(startDate);
		Calendar c1 = Calendar.getInstance();
		c1.setTime(endDate);
		/*long dayDiff = (startDate.getTime() - endDate.getTime())
				/ (1000 * 60 * 60 * 24);*/
		long dayDiffInLong = (startDate.getTime() - endDate.getTime());
		return dayDiffInLong;
	}

	public static Date addTime(Date date1, Date date2) {
		Calendar c = Calendar.getInstance();
		c.setTime(date1);
		Calendar c1 = Calendar.getInstance();
		c1.setTime(date2);
		int hour = c1.get(Calendar.HOUR_OF_DAY);
		int min = c1.get(Calendar.MINUTE);
		int sec = c1.get(Calendar.SECOND);
		c.set(Calendar.HOUR_OF_DAY, hour);
		c.set(Calendar.MINUTE, min);
		c.set(Calendar.SECOND, sec);
		c.set(Calendar.MILLISECOND,0);
		return c.getTime();
	}

	public static Date addDateWithLong(Date date, long dayInLong) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		Calendar c1 = Calendar.getInstance();
		c1.setTimeInMillis(c.getTimeInMillis()+dayInLong);
		return c1.getTime();
	}

	public static long dateDiff(Date date1, Date date2) {
		Calendar dateTime1 = Calendar.getInstance();
		dateTime1.setTime(date1);
		Calendar dateTime2 = Calendar.getInstance();
		dateTime2.setTime(date2);
		Calendar dateWithoutTime1 = Calendar.getInstance();
		dateWithoutTime1.set(Calendar.YEAR, dateTime1.get(Calendar.YEAR));
		dateWithoutTime1.set(Calendar.MONTH, dateTime1.get(Calendar.MONTH));
		dateWithoutTime1.set(Calendar.DAY_OF_MONTH, dateTime1.get(Calendar.DAY_OF_MONTH));
		dateWithoutTime1.set(Calendar.HOUR_OF_DAY, 0);
		dateWithoutTime1.set(Calendar.MINUTE, 0);
		dateWithoutTime1.set(Calendar.SECOND, 0);
		dateWithoutTime1.set(Calendar.MILLISECOND, 0);
		Calendar dateWithoutTime2 = Calendar.getInstance();
		dateWithoutTime2.set(Calendar.YEAR, dateTime2.get(Calendar.YEAR));
		dateWithoutTime2.set(Calendar.MONTH, dateTime2.get(Calendar.MONTH));
		dateWithoutTime2.set(Calendar.DAY_OF_MONTH, dateTime2.get(Calendar.DAY_OF_MONTH));
		dateWithoutTime2.set(Calendar.HOUR_OF_DAY, 0);
		dateWithoutTime2.set(Calendar.MINUTE, 0);
		dateWithoutTime2.set(Calendar.SECOND, 0);
		dateWithoutTime2.set(Calendar.MILLISECOND, 0);
		return dateWithoutTime1.getTimeInMillis() - dateWithoutTime2.getTimeInMillis();
	}

	public static Date getDateFromDateTime(Date date) {
		Calendar dateTime1 = Calendar.getInstance();
		dateTime1.setTime(date);
		Calendar dateWithoutTime1 = Calendar.getInstance();
		int year = dateTime1.get(Calendar.YEAR);
		int month = dateTime1.get(Calendar.MONTH);
		int day = dateTime1.get(Calendar.DAY_OF_MONTH);
		dateWithoutTime1.set(Calendar.YEAR, year);
		dateWithoutTime1.set(Calendar.MONTH, month);
		dateWithoutTime1.set(Calendar.DAY_OF_MONTH, day);
		dateWithoutTime1.set(Calendar.HOUR_OF_DAY, 0);
		dateWithoutTime1.set(Calendar.MINUTE, 0);
		dateWithoutTime1.set(Calendar.SECOND, 0);
		dateWithoutTime1.set(Calendar.MILLISECOND, 0);
		return dateWithoutTime1.getTime();
	}
}
