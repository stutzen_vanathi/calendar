package co.stutzen.event.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.event.common.ConstantUtil;
import co.stutzen.event.entity.Event;
import co.stutzen.event.mapper.UserInfoMapper;
import co.stutzen.event.service.EventService;

@Controller
@RequestMapping("/event")
public class EventController extends SWFController<Event> {

	@Autowired
	public EventService eventService;
	
	@Autowired
	public UserInfoMapper userMapper;

	private static Logger log = LogManager.getRootLogger();

	@Autowired
	public EventController(EventService eventService) {
		super((SWFService<Event>) eventService);
	}

	@ResponseBody
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result save(@RequestBody Event event, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		/*Set IsRepeatEvent, isAllDay = false for Task*/
		if(event.getType() != ConstantUtil.EVENT_TYPE){
			event.setIsRepeatEvent(false);
			event.setIsAllDay(false);
		}
		session.setAttribute("userId", userId);
		event.setCreatedBy((int)session.getAttribute("userId"));
		event.setUpdatedBy((int)session.getAttribute("userId"));
		log.info("save () ===> EVENT ==>" + event);
		event = eventService.create(event);
		eventService.saveEventSchedule(event,false);
		result.setMessage("Event added Successfully..");
		result.setSuccess(true);
		return result;
	}

	@RequestMapping(value = "/modify")
	@ResponseBody
	public Result updateEvent(@RequestBody Event event,
			@RequestParam(value = "status", required = false, defaultValue = "") String status, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		log.info("updateEvent () ===> [status] >>" + status +"[EVENT] >> "+event);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		/*Set IsRepeatEvent, isAllDay = false for Task*/
		if(event.getType() != ConstantUtil.EVENT_TYPE){
			event.setIsRepeatEvent(false);
			event.setIsAllDay(false);
		}
		if(status.equalsIgnoreCase(ConstantUtil.ALL))
			status = ConstantUtil.UPDATE_ALL;
		else if(status.equalsIgnoreCase(ConstantUtil.FOLLOWING_EVENT))
			status = ConstantUtil.UPDATE_FOLLOWING_EVENT;
		else if(status.equalsIgnoreCase(ConstantUtil.SINGLE))
			status = ConstantUtil.UPDATE_SINGLE;
		session.setAttribute("userId", userId);
		event.setUpdatedBy((int)session.getAttribute("userId"));
		eventService.updateEvent(event,status);
		result.setMessage("Event has updated..");
		result.setSuccess(true);
		return result;
	}
	
	@RequestMapping(value = "/findByScheduleId")
	@ResponseBody
	public Result findByScheduleId(@RequestParam int scheduleId) {
		Result result = new Result();
		log.info("findByScheduleId () ===> [scheduleId] >>"+scheduleId);
		result.addData("Event",eventService.findByScheduleId(scheduleId));
		result.setSuccess(true);
		return result;
	}


	@RequestMapping(value = "/deleteEvent")
	@ResponseBody
	public Result updateTaskStatus(
			@RequestParam(value = "status", required = false, defaultValue = "") String status,
			@RequestParam(value = "scheduleId", required = false, defaultValue = "") String scheduleId, HttpSession session) {
		Result result = new Result();
		log.info("updateTaskStatus () ===> [scheduleId] >>"+scheduleId+"[status] >>"+status);
		int userId = 0;
		int eventScheduleId = 0;
		if(!scheduleId.isEmpty())
			eventScheduleId = Integer.parseInt(scheduleId);
		if(status.equalsIgnoreCase(ConstantUtil.ALL))
			status = ConstantUtil.DELETE_ALL;
		else if(status.equalsIgnoreCase(ConstantUtil.FOLLOWING_EVENT))
			status = ConstantUtil.DELETE_FOLLOWING_EVENT;
		else if(status.equalsIgnoreCase(ConstantUtil.SINGLE))
			status = ConstantUtil.DELETE_SINGLE;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		int updateCount = eventService.updateEventStatus(status, eventScheduleId, userId);
		if(updateCount > 0){
			result.setMessage("Event deleted Successfully..");
			result.setSuccess(true);
		}
		else{
			result.setMessage("Event has not deleted Successfully..");
			result.setSuccess(false);	
		}
		return result;
	}

	@RequestMapping(value = "/listAll")
	@ResponseBody
	public Result listAll(
			@RequestParam(value = "assignedId", required = false, defaultValue = "") String assignedId,
			@RequestParam(value = "eventId", required = false, defaultValue = "") String eventId,
			@RequestParam(value = "startDate", required = false, defaultValue = "") String startDate,
			@RequestParam(value = "endDate", required = false, defaultValue = "") String endDate,
			@RequestParam(value = "sourceName", required = false, defaultValue = "") String sourceName,
			@RequestParam(value = "type", required = false, defaultValue = "") String type,
			@RequestParam(value = "status", required = false, defaultValue = "") String status,
			@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "limit", required = false, defaultValue = "") String limit) {
		Result result = new Result();
		log.info("listAll () ===> [assignedId] >>" + assignedId+"[sourceName] >>"+sourceName+"[status] >>"+status+"[eventId] >>"+eventId+"[type] >>"+type+"[startDate]"+startDate+"[endDate]"+endDate);
		int begin = 0;
		int end = 0;
		int userId = 0;
		int id = 0;
		if (endDate.isEmpty()) {
			endDate = startDate;
		}
		if (!assignedId.isEmpty()) {
			userId = Integer.parseInt(assignedId);
		}
		if (!eventId.isEmpty()) {
			id = Integer.parseInt(eventId);
		}
		if (!start.isEmpty()) {
			begin = Integer.parseInt(start);
		}
		if (!limit.isEmpty()) {
			end = Integer.parseInt(limit);
		}
		if(start.isEmpty() && limit.isEmpty()){
			begin = 0;
			end = 100;
		}
			
		result.addData("list", eventService.listAll(userId, id, startDate, endDate, status, sourceName, type, begin, end));
		begin = 0;
		end = 0;
		result.addData("total", eventService.listAll(userId, id, startDate, endDate,status, sourceName, type, begin, end).size());
		result.setSuccess(true);
		return result;
	}
}
