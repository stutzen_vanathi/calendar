package co.stutzen.event.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.event.entity.EventComment;
import co.stutzen.event.mapper.UserInfoMapper;
import co.stutzen.event.service.EventCommentService;

@Controller
@RequestMapping("/comment")
public class EventCommentController extends SWFController<EventComment>{
	
	@Autowired
	public EventCommentService commentService;
	
	@Autowired
	public UserInfoMapper userMapper;

	private static Logger log = LogManager.getRootLogger();

	@Autowired
	public EventCommentController(EventCommentService commentService) {
		super((SWFService<EventComment>)commentService);
	}

	
	@ResponseBody
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result commentSave(@RequestBody EventComment comment,
			@RequestParam(value = "status", required = false, defaultValue = "") String status, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		comment.setCreatedBy((int)session.getAttribute("userId"));
		comment.setUpdatedBy((int)session.getAttribute("userId"));
		log.info("commentSave () ===> [comment] >>"+comment+"[status] >>"+status);
		int insertCount = commentService.insertComment(comment, status);
		if(insertCount >0){
			result.setMessage("Event comment added Successfully..");
			result.setSuccess(true);
		}
		else{
			result.setMessage("Event comment not added Successfully..");
			result.setSuccess(false);
		}
		return result;
	}
	
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Result deleteComment(
			@RequestParam(value = "status", required = false, defaultValue = "") String status,
			@RequestParam(value = "comment", required = false, defaultValue = "") String comment,
			@RequestParam(value = "scheduleId", required = false, defaultValue = "") String scheduleId,
			@RequestParam(value = "eventId", required = false, defaultValue = "") String eventId,
			@RequestParam(value = "id", required = false, defaultValue = "") String id, HttpSession session) {
		Result result = new Result();
		log.info("deleteComment () ===> [comment] >>"+comment+"[status] >>"+status+"[scheduleId] >>"+scheduleId+"[eventId] >>"+eventId+"[commentId] >>"+id);
		int userId = 0;
		int commentId = 0;
		int eventScheduleId = 0;
		int taskOrEventId = 0;
		if(!scheduleId.isEmpty())
			eventScheduleId = Integer.parseInt(scheduleId);
		if(!id.isEmpty())
			commentId = Integer.parseInt(id);
		if(!eventId.isEmpty())
			taskOrEventId = Integer.parseInt(eventId);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		int deleteCount = commentService.deleteComment(comment, status, commentId, taskOrEventId, eventScheduleId, userId);
		if(deleteCount > 0){
			result.setMessage("Comment has deleted successfully");
			result.setSuccess(true);
		}
		else{
			result.setMessage("Comment has not deleted successfully");
			result.setSuccess(false);
			} 
		return result;
	}

}
