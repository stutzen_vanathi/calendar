package co.stutzen.event.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.event.entity.EventFollower;
import co.stutzen.event.mapper.UserInfoMapper;
import co.stutzen.event.service.EventFollowerService;

@Controller
@RequestMapping("/follower")
public class EventFollowerController extends SWFController<EventFollower> {

	@Autowired
	public EventFollowerService followerService;
	
	@Autowired
	public UserInfoMapper userMapper;

	private static Logger log = LogManager.getRootLogger();

	@Autowired
	public EventFollowerController(EventFollowerService followerService) {
		super((SWFService<EventFollower>) followerService);
	}


	@ResponseBody
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result followerSave(@RequestBody List<EventFollower> followers, 			
	@RequestParam(value = "status", required = false, defaultValue = "") String status, HttpSession session) {
		Result result = new Result();
		log.info("followerSave () ===> [status] >> "+status);
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		int insertcount = followerService.insertFollower(followers, status, userId);
		if(insertcount >0){
		result.setMessage("Event follower added Successfully..");
		result.setSuccess(true);
		}
		else{
			result.setMessage("Event follower not added Successfully..");
			result.setSuccess(false);	
		}
		return result;
	}
	
	@RequestMapping(value = "/deleteFollower")
	@ResponseBody
	public Result deleteFollower(
			@RequestParam(value = "followerId", required = false, defaultValue = "") String followerId,
			@RequestParam(value = "scheduleId", required = false, defaultValue = "") String scheduleId,
			@RequestParam(value = "eventId", required = false, defaultValue = "") String eventId,
			@RequestParam(value = "status", required = false, defaultValue = "") String status, HttpSession session) {
		Result result = new Result();
		log.info("deleteFollower () ===> [status] >> "+status+"[followerId] >>"+followerId+"[scheduleId] >>"+scheduleId+"[eventId] >>"+eventId);
		int userId = 0;
		int eventOrTaskId = 0;
		int eventScheduleId = 0;
		int id = 0;
		if(!followerId.isEmpty())
			id = Integer.parseInt(followerId);
		if(!scheduleId.isEmpty())
			eventScheduleId = Integer.parseInt(scheduleId);
		if(!eventId.isEmpty())
			eventOrTaskId = Integer.parseInt(eventId);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		int deleteCount =  followerService.deleteFollower(id, eventOrTaskId, eventScheduleId, userId, status);
		if(deleteCount > 0){
			result.setMessage("Follower has deleted successfully");
			result.setSuccess(true);
		}else{
			result.setMessage("Follower has not deleted successfully");
			result.setSuccess(false);
		}
		return result;
	}

	@RequestMapping(value = "/followerId")
	@ResponseBody
	public Result taskByFollowerId(
			@RequestParam(value = "followerId", required = false, defaultValue = "") String followerId,
			@RequestParam(value = "startDate", required = false, defaultValue = "") String startDate,
			@RequestParam(value = "endDate", required = false, defaultValue = "") String endDate,
			@RequestParam(value = "status", required = false, defaultValue = "") String status,
			@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "limit", required = false, defaultValue = "") String limit) {
		Result result = new Result();
		log.info("taskByFollowerId () ===> [status] >> "+status+"[followerId] >>"+followerId+"[startDate] >>"+startDate+"[endDate] >>"+endDate);
		int begin = 0;
		int end = 0;
		int id = 0;
		if (endDate.isEmpty()) {
			endDate = startDate;
		}
		if (!followerId.isEmpty()) {
			id = Integer.parseInt(followerId);
		}
		if (!start.isEmpty()) {
			begin = Integer.parseInt(start);
		}
		if (!limit.isEmpty()) {
			end = Integer.parseInt(limit);
		}
		result.addData("list", followerService.findByFollowerId(id, startDate, endDate, status,begin, end));
		begin = 0;
		end = 0;
		result.addData("total", followerService.findByFollowerId(id, startDate, endDate, status, begin, end).size());
		result.setSuccess(true);
		return result;
	}
}
