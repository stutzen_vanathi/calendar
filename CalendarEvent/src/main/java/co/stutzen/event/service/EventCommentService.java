package co.stutzen.event.service;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.event.entity.EventComment;

public interface EventCommentService extends SWFService<EventComment>{
	
	int insertComment(EventComment follower, String status);

	int deleteComment(String comment, String status, int id, int eventId, int scheduleId, int userId);


}
