package co.stutzen.event.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.event.common.ConstantUtil;
import co.stutzen.event.common.DateFormatUtil;
import co.stutzen.event.entity.Event;
import co.stutzen.event.entity.EventFollower;
import co.stutzen.event.entity.EventFollowerExample;
import co.stutzen.event.entity.EventFollowerJSON;
import co.stutzen.event.entity.EventJSON;
import co.stutzen.event.entity.EventSchedule;
import co.stutzen.event.entity.EventScheduleJSON;
import co.stutzen.event.mapper.EventFollowerMapper;
import co.stutzen.event.mapper.EventScheduleMapper;
import co.stutzen.event.service.EventFollowerService;

@Service
public class EventFollowerServiceImpl extends SWFServiceImpl<EventFollower> implements EventFollowerService {

	@Autowired
	public EventFollowerMapper followerMapper;
	
	@Autowired
	public EventScheduleMapper scheduleMapper;
	
	private static Logger log = LogManager.getRootLogger();
	
	@Override
	public List<EventFollowerJSON> findByFollowerId(int followerId, String startDate, String endDate,String status,int begin, int end) {
		List<EventFollower> list = null;
		Date from = null;
		Date to = null;
		if (!(startDate.isEmpty()) && !(endDate.isEmpty())) {
			from = DateFormatUtil.getDateObject(startDate);
			to = DateFormatUtil.getDateObject(endDate);
		}
		if (begin == 0 && end == 0) {
			list = followerMapper.findByFollowerId(followerId, from, to,status);
		} else {
			list = followerMapper.findByFollowerIdIdWithLimit(followerId, from,
					to,status, new EventFollowerExample(), new RowBounds(begin, end));
		}
		return eventListByFollower(list);
	}

	/**
	 * Get task list by follower
	 */
	private List<EventFollowerJSON> eventListByFollower(List<EventFollower> list) {
		List<EventFollowerJSON> followerList = new ArrayList<EventFollowerJSON>();
		for (EventFollower follower : list) {
			EventFollowerJSON json = new EventFollowerJSON();
			json.setFollowerId(follower.getFollowerId());
			if (follower.getFollower() != null) {
				json.setFollowerName(follower.getFollower().getName());
			}
			json.setSchedules(scheduleList(list, follower.getFollowerId()));
			followerList.add(json);
			return followerList;
		}
		return followerList;
	}

	/**
	 * Get schedule list by taskId
	 */
	private List<EventScheduleJSON> scheduleList(List<EventFollower> list,
			int followerId) {
		List<EventScheduleJSON> scheduleList = new ArrayList<EventScheduleJSON>();
		for (EventFollower follower : list) {
			if (follower.getFollowerId() == followerId) {
				for (EventSchedule schedule : follower.getSchedules()) {
					scheduleList.add(getEventSchedule(schedule));
				}
			}
		}
		return scheduleList;
	}

	private EventScheduleJSON getEventSchedule(EventSchedule schedule) {
		EventScheduleJSON json = new EventScheduleJSON();
		json.setId(schedule.getId());
		json.setEventId(schedule.getEventId());
		/*json.setTaskStart(schedule.getEventStart());*/
		json.setEventEnd(schedule.getEventEnd());
		json.setComments(schedule.getComments());
		json.setStatus(schedule.getStatus());
		json.setRepeatType(schedule.getRepeatType());
		json.setCommentList(schedule.getEventComments());
		if (schedule.getEvent() != null) {
			json.setEvent(getEvent(schedule.getEvent()));
		}
		if (schedule.getFollowerList() != null) {
			json.setFollowerList(followerList(schedule.getFollowerList()));
		}
		return json;
	}

	/**
	 * Get Event
	 */
	private EventJSON getEvent(Event task) {
		EventJSON json = new EventJSON();
		json.setAssignedFromId(task.getAssignedFromId());
		json.setId(task.getId());
		if (task.getAssignedFrom() != null) {
			json.setAssignedFromName(task.getAssignedFrom().getName());
		}
		json.setAssignedFromId(task.getAssignedFromId());
		json.setAssignedType(task.getAssignedType());
/*		json.setDescription(task.getDescription());
		json.setTitle(task.getTitle());
*/		return json;
	}

	/**
	 * Get follower list for particular task schedule
	 */
	private List<EventFollower> followerList(List<EventFollower> list) {
		List<EventFollower> followers = new ArrayList<EventFollower>();
		for (EventFollower follower : list) {
			EventFollower followerObj = new EventFollower();
			followerObj.setId(follower.getId());

			if (follower.getFollower() != null) {
				followerObj.setFollowerName(follower.getFollower().getName());
			}
			followerObj.setEventScheduleId(follower.getEventScheduleId());
			followers.add(followerObj);
		}
		return followers;
	}

	@Override
	public int insertFollower(List<EventFollower> followers, String status, int userId) {
		List<EventSchedule> scheduleList = new ArrayList<EventSchedule>();
		int insertCount = 0;
		for(EventFollower follower : followers){
			follower.setCreatedBy(userId);
			follower.setUpdatedBy(userId);
			log.info("1. insertFollower () ===> [Follower] >>"+follower + "[status] >>"+status);
			if(!status.isEmpty()){
				if (status.equalsIgnoreCase(ConstantUtil.UPDATE_ALL)) {
					scheduleList = scheduleMapper.getEventByEventId(follower.getEventId(), ConstantUtil.CREATED);
					insertCount = saveFollower(scheduleList, follower);
				}else if (status.equalsIgnoreCase(ConstantUtil.UPDATE_FOLLOWING_EVENT)) {
					scheduleList = scheduleMapper.getFollowingEventByEventIdAndScheduleId(follower.getEventId(), follower.getEventScheduleId(), ConstantUtil.CREATED);
					insertCount = saveFollower(scheduleList, follower);
				}else if (status.equalsIgnoreCase(ConstantUtil.UPDATE_SINGLE) || status.isEmpty()){
					EventFollower followerFromDb = followerMapper.selectFollower(follower.getEventScheduleId(), follower.getEventId(), follower.getFollowerId());
					if(followerFromDb == null)
						insertCount = followerMapper.insert(follower);
				}
				}
			else{
				insertCount = followerMapper.insert(follower);
			}
			}
		log.info("2. insertFollower () ===> [insertCount] >>"+insertCount);
		return insertCount;
	}

	private int saveFollower(List<EventSchedule> list, EventFollower follower) {
		int insertCount = 0;
		if (list != null && list.size() > 0) {
			for (EventSchedule schedule : list) {
				log.info("1. saveFollower () ===> [schedule] >>"+schedule);
				EventFollower followerFromDb = followerMapper.selectFollower(schedule.getId(), schedule.getEventId(), follower.getFollowerId());
				follower.setEventId(schedule.getEventId());
				follower.setEventScheduleId(schedule.getId());
				log.info("2. saveFollower () ===> [followerFromDb] >>"+followerFromDb +"[follower] >>"+follower);
				if(followerFromDb == null)
					insertCount =	followerMapper.insert(follower);
			}
		} 
		log.info("3. saveFollower () ===> [insertCount] >>"+insertCount);
		return insertCount;
	}
	
	@Override
	public int deleteFollower(int followerId, int eventId, int scheduleId, int userId, String status) {
		log.info("1. deleteFollower () ===> [eventId] >>"+eventId+"[followerId] >>"+followerId+"[scheduleId] >>"+scheduleId+"[status] >>"+status);
		int deleteCount = 0;
		List<EventSchedule> scheduleList = null;
		if (!status.isEmpty() && status.equalsIgnoreCase(ConstantUtil.DELETE_ALL)) {
			scheduleList = scheduleMapper.getEventByEventId(eventId, ConstantUtil.CREATED);
			deleteCount = deleteFollower(scheduleList, followerId, userId);
		}else if (!status.isEmpty() && status.equalsIgnoreCase(ConstantUtil.DELETE_FOLLOWING_EVENT)) {
			scheduleList = scheduleMapper.getFollowingEventByEventIdAndScheduleId(eventId, scheduleId, ConstantUtil.CREATED);
			deleteCount = deleteFollower(scheduleList, followerId, userId);
		}else if ((!status.isEmpty() && status.equalsIgnoreCase(ConstantUtil.DELETE_SINGLE)) || status.isEmpty()){
			deleteCount = followerMapper.deleteFollower(followerId, ConstantUtil.IS_ACTIVE, userId, eventId, scheduleId);
		}
		log.info("2. deleteFollower () ===> [deleteCount] >>"+deleteCount);
		return deleteCount;
	}

	private int deleteFollower(List<EventSchedule> list,int followerId,int userId ) {
		int deleteCount = 0;
		if (list != null && list.size() > 0) {
			for (EventSchedule schedule : list) {
				log.info("1. deleteFollower () ===> [schedule] >>"+schedule+"[followerId] >>"+followerId);
				deleteCount =followerMapper.deleteFollower(followerId, ConstantUtil.IS_ACTIVE, userId, schedule.getEventId(), schedule.getId() );
			}
		} 
		return deleteCount;
	}
}
