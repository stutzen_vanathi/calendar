package co.stutzen.event.service;

import java.util.List;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.event.entity.EventFollower;
import co.stutzen.event.entity.EventFollowerJSON;

public interface EventFollowerService extends SWFService<EventFollower> {

	List<EventFollowerJSON> findByFollowerId(int id, String startDate,
			String endDate, String status, int begin, int end);

	int insertFollower(List<EventFollower> followers, String status, int userId);

	int deleteFollower(int followerId, int eventId, int scheduleId, int userId, String status);

}
