package co.stutzen.event.service.impl;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.event.common.ConstantUtil;
import co.stutzen.event.entity.EventComment;
import co.stutzen.event.entity.EventSchedule;
import co.stutzen.event.mapper.EventCommentMapper;
import co.stutzen.event.mapper.EventScheduleMapper;
import co.stutzen.event.service.EventCommentService;

@Service
public class EventCommentServiceImpl extends SWFServiceImpl<EventComment> implements EventCommentService{
	
	@Autowired
	public EventCommentMapper commentMapper;
	
	@Autowired
	public EventScheduleMapper scheduleMapper;

	private static Logger log = LogManager.getRootLogger();
	
	@Override
	public int insertComment(EventComment comment, String status) {
		log.info("insertComment () ===> [comment] >>"+comment+"[status] >>"+status);
		List<EventSchedule> scheduleList = null;
		int insertCount = 0;
		if (!status.isEmpty() && status.equalsIgnoreCase(ConstantUtil.UPDATE_ALL)) {
			scheduleList = scheduleMapper.getEventByEventId(comment.getEventId(), ConstantUtil.CREATED);
			insertCount = saveComment(scheduleList, comment);
		}else if (!status.isEmpty() && status.equalsIgnoreCase(ConstantUtil.UPDATE_FOLLOWING_EVENT)) {
			scheduleList = scheduleMapper.getFollowingEventByEventIdAndScheduleId(comment.getEventId(), comment.getEventScheduleId(), ConstantUtil.CREATED);
			insertCount =  saveComment(scheduleList, comment);
		}else if ((!status.isEmpty() && status.equalsIgnoreCase(ConstantUtil.UPDATE_SINGLE)) || status.isEmpty()){
			insertCount =  commentMapper.insert(comment);
		}
		log.info("2. insertComment () ===> [insertCount] >>"+insertCount);
		return insertCount;
	}

	private int saveComment(List<EventSchedule> list, EventComment comment) {
		int insertCount = 0;
		if (list != null && list.size() > 0) {
			for (EventSchedule schedule : list) {
				comment.setEventId(schedule.getEventId());
				comment.setEventScheduleId(schedule.getId());
				log.info("1. saveComment () ===> [comment] >>"+comment);
				insertCount =commentMapper.insert(comment);
			}
		} 
		log.info("2. saveComment () ===> [insertCount] >>"+insertCount);
		return insertCount;
	} 
			

	@Override
	public int deleteComment(String comment, String status, int id, int eventId, int scheduleId, int userId) {
		int deleteCount = 0;
		List<EventSchedule> scheduleList = null;
		log.info("1. deleteComment () ===> [comment] >>"+comment+"[status] >>"+status+"[id] >> "+id+ "[eventId] >>"+eventId+"[scheduleId] >>"+scheduleId);
		if (status.equalsIgnoreCase(ConstantUtil.DELETE_ALL)) {
			scheduleList = scheduleMapper.getEventByEventId(eventId, ConstantUtil.CREATED);
			deleteCount = deleteComment(scheduleList,userId, comment);
		}else if (status.equalsIgnoreCase(ConstantUtil.DELETE_FOLLOWING_EVENT)) {
			scheduleList = scheduleMapper.getFollowingEventByEventIdAndScheduleId(eventId, scheduleId, ConstantUtil.CREATED);
			deleteCount = deleteComment(scheduleList, userId, comment);
		}else if (status.equalsIgnoreCase(ConstantUtil.DELETE_SINGLE) || status.isEmpty()){
			deleteCount =  commentMapper.deleteCommentById(id, ConstantUtil.IS_ACTIVE, userId);
		}
		log.info("2. deleteComment () ===> [deleteCount] >>"+deleteCount);
		return deleteCount;
	}

	private int deleteComment(List<EventSchedule> list,int userId, String comment) {
		int deleteCount = 0;
		if (list != null && list.size() > 0) {
			for (EventSchedule schedule : list) {
				log.info("1. deleteComment () ===> [schedule] >>"+schedule+"[comment] >>"+comment);
				deleteCount = commentMapper.deleteComment(ConstantUtil.IS_ACTIVE, userId, comment, schedule.getEventId(), schedule.getId());
			}
		} 
		return deleteCount;
	}
}
