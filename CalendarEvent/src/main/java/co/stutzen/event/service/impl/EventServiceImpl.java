package co.stutzen.event.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.ibatis.session.RowBounds;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.event.common.CUtil;
import co.stutzen.event.common.ConstantUtil;
import co.stutzen.event.common.DateFormatUtil;
import co.stutzen.event.entity.Event;
import co.stutzen.event.entity.EventComment;
import co.stutzen.event.entity.EventExample;
import co.stutzen.event.entity.EventFollower;
import co.stutzen.event.entity.EventFollowerExample;
import co.stutzen.event.entity.EventJSON;
import co.stutzen.event.entity.EventSchedule;
import co.stutzen.event.entity.EventScheduleJSON;
import co.stutzen.event.entity.UserInfo;
import co.stutzen.event.mapper.EventCommentMapper;
import co.stutzen.event.mapper.EventFollowerMapper;
import co.stutzen.event.mapper.EventMapper;
import co.stutzen.event.mapper.EventScheduleMapper;
import co.stutzen.event.service.EventService;

@Service
public class EventServiceImpl extends SWFServiceImpl<Event> implements EventService {

	@Autowired
	public EventMapper eventMapper;

	@Autowired
	public EventScheduleMapper scheduleMapper;

	@Autowired
	public EventCommentMapper commentMapper;

	@Autowired
	public EventFollowerMapper followerMapper;

	private static Logger log = LogManager.getRootLogger();
	
	@Override
	public void updateEvent(Event event, String status) {
		log.info("1. updateEvent () ==> [event] >> "+event + "[status] >>"+status);
		eventMapper.updateAllDay(event);
		/*  Check user given input as event type else task type */
		if (event.getType() == ConstantUtil.EVENT_TYPE) {
			Event eventFromDB = null;
			EventSchedule scheduleFromDB = null;
			/* Get event and schedule details from DB to compare the date */
			if (!status.isEmpty()) {
				eventFromDB = eventMapper.selectByPrimaryKey(event.getId());
				scheduleFromDB = scheduleMapper.selectByPrimaryKey(event.getSchedule().getId());
			}
			log.info("2. updateEvent () ==> [eventFromDB] >> "+eventFromDB);
			log.info("3. updateEvent () ==> [scheduleFromDB] >> "+scheduleFromDB);
			/* Update all kind of event (Update Event tbl alone) */
			updateAllEvents(event, status);

			/* Update for Event schedule (Update for Event schedule tbl) */
			if (event.getIsRepeatEvent() && scheduleFromDB != null) {
				
				/* Single event don't have Repeat event. So We are checking NOT condition. We need to update Following & All event only*/
				
				if (!status.equalsIgnoreCase(ConstantUtil.UPDATE_SINGLE)) {
					if (event.getSchedule().getOccurrences() == 0)
						event.getSchedule().setEventEnd(event.getSchedule().getRepeatEventEnd());
				}
				/*
				 * Check user given date with DB's event & schedule date
				 */
				boolean isTrue = false;
				if (!status.equalsIgnoreCase(ConstantUtil.SINGLE_EVENT))
					isTrue = checkEventDate(eventFromDB, scheduleFromDB, event, status);
				/*
				 * If user has not changed date OR status = single, else date
				 * has been changed
				 */
				if (isTrue || status.equalsIgnoreCase(ConstantUtil.SINGLE_EVENT)) {
					setScheduleDetails(event, status);
				} else {
					if (status.equalsIgnoreCase(ConstantUtil.UPDATE_FOLLOWING_EVENT)) {
						EventSchedule schedule = scheduleMapper.getBeforeRepeatOrder(event.getSchedule().getId(),event.getId(),
										ConstantUtil.CREATED);
						ConstantUtil.EVENT_ID = 0;
						Event eventObj = null;
						if (schedule != null) {
							eventObj = saveEvent(event);
							eventMapper.insert(eventObj);
							ConstantUtil.EVENT_ID = eventObj.getId();
						}
						/*
						 * Set Event Repeat start date as Event start date.
						 * Because this event will split from main event
						 */
						event.getSchedule().setRepeatEventStart(DateFormatUtil.getDateFromDateTime(event.getEventStart()));
					}
					event.getSchedule().setTitle(event.getTitle());
					event.getSchedule().setProjectName(CUtil.CheckNull(event.getProjectName()));
					event.getSchedule().setDescription(CUtil.CheckNull(event.getDescription()));
					event.getSchedule().setComments(CUtil.CheckNull(event.getComment()));
					event.getSchedule().setUpdatedBy(event.getUpdatedBy());
					if (!(event.getSchedule().getRepeatType().equalsIgnoreCase(ConstantUtil.WEEKLY)))
						event.getSchedule().setRepeatOn("");
					checkEventType(event, true, status);
					ConstantUtil.EVENT_ID = 0;
				}
			} else {
				/**
				 * User has added repeat event for already created event
				 */
				/*
				 * if (event.getIsRepeatEvent()) saveEventSchedule(event, true);
				 * else
				 */
				if (!status.isEmpty())
					scheduleMapper.updateByPrimaryKey(updateSchedule(event));
			}
		} else {
			eventMapper.updateById(event);
			EventSchedule schedule = scheduleMapper.scheduleByEventId(event.getId());
			schedule.setComments(CUtil.CheckNull(event.getComment()));
			schedule.setUpdatedBy(event.getUpdatedBy());
			schedule.setDescription(CUtil.CheckNull(event.getDescription()));
			schedule.setProjectName(CUtil.CheckNull(event.getProjectName()));
			schedule.setTitle(CUtil.CheckNull(event.getTitle()));
			schedule.setEventId(event.getId());
			schedule.setEventEnd(event.getEventEnd());
			schedule.setRepeatEventEnd(event.getEventEnd());
			schedule.setRepeatEventStart(event.getEventEnd());
			schedule.setRepeatOn("");
			scheduleMapper.updateByPrimaryKey(schedule);
		}
	}

	private void setScheduleDetails(Event event, String status) {
		event.getSchedule().setEventId(event.getId());
		event.getSchedule().setTitle(event.getTitle());
		event.getSchedule().setDescription(event.getDescription());
		event.getSchedule().setStatus(ConstantUtil.CREATED);
		event.getSchedule().setRepeatType(event.getSchedule().getRepeatType());
		event.getSchedule().setProjectName(CUtil.CheckNull(event.getProjectName()));
		event.getSchedule().setUpdatedBy(event.getUpdatedBy());
		event.getSchedule().setComments(CUtil.CheckNull(event.getComment()));
		if (status.equalsIgnoreCase(ConstantUtil.UPDATE_SINGLE)) {
			event.getSchedule().setOccurrences(event.getSchedule().getOccurrences());
			event.getSchedule().setEventEnd(event.getSchedule().getEventEnd());
			event.getSchedule().setRepeatEventEnd(event.getEventEnd());
			event.getSchedule().setRepeatEventStart(event.getEventStart());
		}
		event.getSchedule().setEventEnd(event.getSchedule().getRepeatEventEnd());
		updateRepeatEvent(event, status);
	}

	private boolean checkEventDate(Event eventFromDB,EventSchedule scheduleFromDB, Event event, String status) {
		boolean isResult = false;
		boolean eventStart = scheduleFromDB.getRepeatEventStart().equals(event.getEventStart());
		boolean eventEnd = scheduleFromDB.getRepeatEventEnd().equals(event.getEventEnd());
		boolean occurrence = false;
		boolean repeatEventEnd = false;
		boolean isRepeatOnSame = true;
		if (scheduleFromDB.getEventEnd() != null && event.getSchedule().getRepeatEventEnd() != null)
			repeatEventEnd = scheduleFromDB.getEventEnd().equals(event.getSchedule().getRepeatEventEnd());
		if (event.getSchedule().getOccurrences() != 0)
			occurrence = event.getSchedule().getOccurrences() == scheduleFromDB.getOccurrences();
		if (scheduleFromDB.getRepeatType().equalsIgnoreCase(ConstantUtil.WEEKLY))
			isRepeatOnSame = checkRepeatOn(event, scheduleFromDB.getRepeatOn());
		/* If user may change event repeat start, end date, occurrence and repeat on */
		log.info("1. checkEventDate () ===> [eventStart] >>"+eventStart+"[eventEnd] >>"+eventEnd+"[repeatEventEnd] >>"+repeatEventEnd+"[occurrence] >>"+occurrence+"[isRepeatOnSame] >> "+isRepeatOnSame);
		if (!(repeatEventEnd || occurrence)) {
			isResult = false;
			return isResult;
		}
		if (!eventStart) {
			isResult = false;
			return isResult;
		}
		if (!eventEnd) {
			isResult = false;
			return isResult;
		}
		if (!isRepeatOnSame) {
			isResult = false;
			return isResult;
		}
		/*  If user may not change event repeat start, end date, occurrence and repeat on */
		if (((repeatEventEnd || occurrence) && (eventStart || eventEnd))|| status.equalsIgnoreCase(ConstantUtil.UPDATE_SINGLE) || isRepeatOnSame) {
			isResult = true;
		}
		log.info("2. checkEventDate () ===> [isResult] >>"+isResult);
		return isResult;
	}

	/**
	 * update for single, following, all event & no status (No repeat event) (Update for Event tbl)
	 */
	private void updateAllEvents(Event event, String status) {
		log.info("1. updateAllEvents () ===> [event] >>"+event+"[status] >>"+status);
		if ((status.equalsIgnoreCase(ConstantUtil.UPDATE_SINGLE) || status.equalsIgnoreCase(ConstantUtil.UPDATE_FOLLOWING_EVENT) || status.isEmpty())) {
			if (status.isEmpty()) {
				if (event.getIsRepeatEvent() == false)
					eventMapper.updateById(event);
				if (event.getIsRepeatEvent()) {
					EventSchedule s = scheduleMapper.scheduleByEventId(event.getId());
					event.getSchedule().setId(s.getId());
					saveEventSchedule(event, true);
				} else
					updateScheduleById(event);
			}
		}
		}

	private void updateScheduleById(Event event) {
		EventSchedule schedule = scheduleMapper.selectByPrimaryKey(event.getSchedule().getId());
		if(schedule != null){
		schedule.setEventId(event.getId());
		schedule.setId(schedule.getId());
		schedule.setDescription(event.getDescription());
		schedule.setEventEnd(event.getEventEnd());
		schedule.setRepeatEventEnd(event.getEventEnd());
		schedule.setRepeatEventStart(event.getEventStart());
		schedule.setTitle(event.getTitle());
		schedule.setOccurrences(0);
		if (event.getType() == ConstantUtil.EVENT_TYPE)
			schedule.setRepeatType(ConstantUtil.NO_REPEAT_TYPE);
		if (event.getType() == ConstantUtil.TASK_TYPE)
			schedule.setProjectName(event.getProjectName());
		log.info("updateScheduleById () ==> [schedule] >> "+schedule);
		scheduleMapper.updateEventScheduleById(schedule);
		}
	}

	private EventSchedule updateSchedule(Event event) {
		EventSchedule schedule = new EventSchedule();
		schedule.setId(event.getSchedule().getId());
		schedule.setEventId(event.getId());
		schedule.setTitle(event.getTitle());
		schedule.setDescription(event.getDescription());
		schedule.setStatus(ConstantUtil.CREATED);
		schedule.setRepeatType(ConstantUtil.NO_REPEAT_TYPE);
		schedule.setOccurrences(0);
		if (event.getType() == ConstantUtil.TASK_TYPE)
			schedule.setProjectName(event.getProjectName());
		schedule.setStatus(ConstantUtil.CREATED);
		schedule.setIsActive(event.getIsActive());
		schedule.setRepeatEventStart(event.getEventStart());
		schedule.setRepeatEventEnd(event.getEventEnd());
		schedule.setIsSingle(true);
		schedule.setEventEnd(event.getEventEnd());
		log.info("1. updateSchedule () ==> [schedule] >> "+schedule);
		return schedule;
	}

	/**
	 * This method will call If repeat type is Weekly
	 */
	private boolean checkRepeatOn(Event event, String repeatOn) {
		boolean isSuccess = false;
		List<Integer> i = new ArrayList<Integer>();
		String[] repeatOnList = repeatOn.split(",");
		for (String s : repeatOnList) {
			log.info("1. checkRepeatOn () ===> [RepeatOn] >>"+ s);
			i.add(Integer.parseInt(s));
		}
		log.info("1. checkRepeatOn () ===> [Repeat Type] >>"+event.getSchedule().getRepeatType());
		if (event.getSchedule().getRepeatType() != null && event.getSchedule().getRepeatType().equalsIgnoreCase(ConstantUtil.WEEKLY)) {
			isSuccess = i.equals(event.getSchedule().getRepeatOnList());
		}
		return isSuccess;
	}

	private Event saveEvent(Event event) {
		Event e1 = new Event();
		e1.setType(event.getType());
		e1.setAssignedFromId(event.getAssignedFromId());
		e1.setAssignedToId(event.getAssignedToId());
		e1.setAssignedType(event.getAssignedType());
		e1.setEventStart(event.getEventStart());
		e1.setEventEnd(event.getEventEnd());
		e1.setComment(event.getComment());
		e1.setIsRepeatEvent(event.getIsRepeatEvent());
		e1.setCreatedBy(event.getUpdatedBy());
		e1.setUpdatedBy(event.getUpdatedBy());
		e1.setSourceName(event.getSourceName());
		e1.setIsAllDay(event.getIsAllDay());
		e1.setIsActive(true);
		log.info("1. saveEvent () ===> [event] >>"+event);
		return e1;
	}

	private Event addTime(Event event, boolean isAddTime) {
		Date date = null;
		long dateInLong = Math.abs(DateFormatUtil.dateDiff(event.getEventStart(), event.getEventEnd()));
		/*
		 * If part is used to add the time from event (start & end) to repeat event (start & end)
		 */
		if (isAddTime) {
			date = DateFormatUtil.addTime(event.getSchedule().getRepeatEventStart(), event.getEventStart());
			/* Add startDate With EndDateTime */
			Date startDateWithEndDateTime = DateFormatUtil.addTime(event.getSchedule().getRepeatEventStart(), event.getEventEnd());
			event.getSchedule().setRepeatEventEnd(DateFormatUtil.addDateWithLong(startDateWithEndDateTime,dateInLong));
			event.getSchedule().setRepeatEventStart(date);
		} else {
			event.getSchedule().setRepeatEventEnd(event.getEventEnd());
			event.getSchedule().setRepeatEventStart(event.getEventStart());
		}
		log.info("addTime () ===> EVENT >> "+event);
		return event;
	}

	/**
	 * Update event schedule details if event schedule start date and end date is not changed by user
	 */
	private void updateRepeatEvent(Event event, String status) {
		/*  update all and following the repeat event which has status = created */
		log.info("updateRepeatEvent () ===> [event] >>"+event+"[status] >>"+status);
		if (status.equalsIgnoreCase(ConstantUtil.UPDATE_ALL)) {
			event.getSchedule().setStatus(ConstantUtil.CREATED);
			event.getSchedule().setIsSingle(false);
			scheduleMapper.updateEventScheduleAll(event.getSchedule());
		} else if (status.equalsIgnoreCase(ConstantUtil.UPDATE_FOLLOWING_EVENT)) {
			event.getSchedule().setStatus(ConstantUtil.CREATED);
			event.getSchedule().setIsSingle(false);
			scheduleMapper.updateEventScheduleFollowing(event.getSchedule());
		} else if (status.equalsIgnoreCase(ConstantUtil.UPDATE_SINGLE)) {
			event.getSchedule().setIsSingle(true);
			scheduleMapper.updateScheduleSingle(event.getSchedule());
		}
	}

	@Override
	public void saveEventSchedule(Event event, boolean isUpdate) {
		log.info("1. saveEventSchedule () ===> [event] >> "+event+"[isUpdate] >>"+isUpdate);
		/*  If user given Input as Event, then if part will be executed,else task type */
		if (event.getType() == ConstantUtil.EVENT_TYPE) { // 1-Event, 2-Task. Repeat Event save.
			if (event.getIsRepeatEvent()) {
				event.getSchedule().setEventId(event.getId());
				event.getSchedule().setIsSingle(false);
				event.getSchedule().setTitle(event.getTitle());
				event.getSchedule().setDescription(event.getDescription());
				event.getSchedule().setCreatedBy(event.getCreatedBy());
				event.getSchedule().setUpdatedBy(event.getUpdatedBy());
				event.getSchedule().setProjectName(CUtil.CheckNull(event.getProjectName()));
				event.getSchedule().setComments(CUtil.CheckNull(event.getComment()));
				event.getSchedule().setStatus(ConstantUtil.CREATED);
				checkEventType(event, isUpdate, ConstantUtil.NO_STATUS);
			} else {
				eventScehduleSave(event); // Event Save
			}
		} else {
			eventScehduleSave(event); // Task save
		}
	}

	private void eventScehduleSave(Event event) {
		EventSchedule schedule = new EventSchedule();
		schedule.setCreatedBy(event.getCreatedBy());
		schedule.setUpdatedBy(event.getUpdatedBy());
		schedule.setEventId(event.getId());
		schedule.setTitle(event.getTitle());
		schedule.setDescription(CUtil.CheckNull(event.getDescription()));
		schedule.setStatus(ConstantUtil.CREATED);
		schedule.setRepeatType(ConstantUtil.NO_REPEAT_TYPE);
		schedule.setProjectName(CUtil.CheckNull(event.getProjectName()));
		if (!event.getIsRepeatEvent()) 
			schedule.setRepeatOn("");
		if(event.getType() == ConstantUtil.TASK_TYPE){
			schedule.setRepeatEventEnd(event.getEventEnd());
			schedule.setRepeatEventStart(event.getEventEnd());
		}
		event.setSchedule(schedule);
		if (event.getType() == ConstantUtil.EVENT_TYPE)
			event = addTime(event, false);
		schedule.setIsSingle(false);
		schedule.setOccurrences(0);
		schedule.setEventEnd(event.getEventEnd());
		schedule.setStatus(ConstantUtil.CREATED);
		schedule.setIsActive(event.getIsActive());
		schedule.setComments(CUtil.CheckNull(event.getComment()));
		log.info("1. eventScehduleSave () ===> [schedule] >> "+schedule);
		scheduleMapper.insert(schedule);
		saveEventCommentAndFollower(event, false);
	}

	private void checkEventType(Event event, boolean isUpdate, String status) {
		if (event.getSchedule().getRepeatType().equalsIgnoreCase(ConstantUtil.DAILY)) {
			log.info("checkEventType () ==> DAILY");
			saveDailyEvent(event, isUpdate, status);
		} else if (event.getSchedule().getRepeatType().equalsIgnoreCase(ConstantUtil.WEEKLY)) {
			log.info("checkEventType () ==> WEEKLY");
			saveWeeklyEvent(event, isUpdate, status);
		} else if (event.getSchedule().getRepeatType().equalsIgnoreCase(ConstantUtil.MONTHLY)) {
			log.info("checkEventType () ==> MONTHLY");
			saveMonthlyEvent(event, isUpdate, status);
		} else if (event.getSchedule().getRepeatType().equalsIgnoreCase(ConstantUtil.YEARLY)) {
			log.info("checkEventType () ==> YEARLY");
			saveYearlyEvent(event, isUpdate, status);
		}
	}

	private long findDayDiff(Date startDate, Date endDate) {
		long dayDiffInLong = DateFormatUtil.dayDiff(startDate, endDate);
		log.info("findDayDiff () ===> [startDate] >> "+startDate+"[endDate] >> "+endDate + "[dayDiffInLong] >>"+dayDiffInLong);
		return dayDiffInLong;
	}

	/**
	 * Save Daily based Event
	 */
	private void saveDailyEvent(Event event, boolean isUpdate, String status) {
		long interval = ConstantUtil.DAY_IN_SECONDS* event.getSchedule().getRepeatEvery();
		log.info("saveDailyEvent () ===> EVENT SCHEDULE >> "+ event.getSchedule());
		if (event.getSchedule().getOccurrences() == 0)
			event.getSchedule().setEventEnd(event.getSchedule().getRepeatEventEnd());
		/*
		 * Find event end date, If user entered occurrence instead of end date
		 */
		if (event.getSchedule().getOccurrences() != 0) {
			long taskDuration = interval* (event.getSchedule().getOccurrences());
			event.getSchedule().setRepeatEventEnd(DateFormatUtil.addSeconds(event.getSchedule().getRepeatEventStart(), taskDuration));
		}
		log.info("saveDailyEvent () ===> EVENT >> "+event +" SCHEDULE START >> "+event.getSchedule().getRepeatEventStart() + " SCHEDULE END >> "+event.getSchedule().getRepeatEventEnd() +"OCCURRENCES >> "+event.getSchedule().getOccurrences() + "STATUS >> "+status + "INTERVAL >> "+interval);
		saveDailyEventSchedule(event,event.getSchedule().getRepeatEventStart(), event.getSchedule().getRepeatEventEnd(), event.getSchedule().getOccurrences(), isUpdate, status, interval);
	}

	/**
	 * Save Daily based Event schedule
	 */
	private Event saveDailyEventSchedule(Event event, Date startDate, Date endDate, int occurrences, boolean isUpdate, String status, long interval) {
		int occurrencesCount = 0;
		int scheduleId = 0;
		int eventSchduleId = 0;
		boolean isFollowerAndcommentSave = true;
		 /* Get schedule id for update, if update = true */
		if (isUpdate) {
			scheduleId = event.getSchedule().getId();
			/* Get Before event schedule record and update end date*/
			EventSchedule e = scheduleMapper.getBeforeRecordWithAsc(event.getSchedule().getId(), event.getId(), (byte) 0, ConstantUtil.CREATED);
			log.info("1. saveDailyEventSchedule () ===> BEFORE SCHEDULE RECORD >>"+e);
			if (e != null)
				eventSchduleId = e.getId();
		}
		while (true) {
			event.getSchedule().setRepeatEventStart(startDate);
			event = addTime(event, true);
			 /* Insert the schedule, if there is no next record found when update= true OR Insert the record when update = false */
			log.info("2. saveDailyEventSchedule () ===> [scheduleId] >> "+scheduleId+ "[isUpdate] >> " + isUpdate +"[status] >> "+status + "[Constant_Event_id] >>"+ConstantUtil.EVENT_ID);
			if (scheduleId == 0 || !isUpdate) {
				int eventId = event.getId();
				if (isUpdate && (status.equalsIgnoreCase(ConstantUtil.UPDATE_ALL) || status.equalsIgnoreCase(ConstantUtil.UPDATE_FOLLOWING_EVENT)))
					if (ConstantUtil.EVENT_ID != 0)
						eventId = ConstantUtil.EVENT_ID;
				event.getSchedule().setEventId(eventId);
				event.getSchedule().setRepeatOn(CUtil.CheckNull(event.getSchedule().getRepeatOn()));
				event.getSchedule().setIsSingle(ConstantUtil.IS_SINGLE);
				if(scheduleId == 0 && isUpdate == true)
					event.getSchedule().setCreatedBy(event.getUpdatedBy());
				event.getSchedule().setUpdatedBy(event.getUpdatedBy());
				log.info("3. saveDailyEventSchedule () ===> Event Schedule >> "+event.getSchedule());
				scheduleMapper.insert(event.getSchedule());
				if (!isUpdate)
					saveEventCommentAndFollower(event, isUpdate);
			}
			log.info("4. saveDailyEventSchedule () ===> [isUpdate] >> "+isUpdate +"[scheduleId] >>"+scheduleId);
			/*This will execute when isUpate = true */
			if (isUpdate && scheduleId != 0) {
				event.getSchedule().setId(scheduleId);
				scheduleId = updateEventSchedule(event.getSchedule(), status, eventSchduleId, event.getUpdatedBy());
				eventSchduleId = 0;
				if (isFollowerAndcommentSave)
					saveEventCommentAndFollower(event, isUpdate);
				isFollowerAndcommentSave = false;
			}
			/* Find next event occurrence date*/
			startDate = DateFormatUtil.addSeconds(startDate, interval);
			occurrencesCount++;
			Date sDate = DateFormatUtil.getDateFromDateTime(startDate);
			Date eDate = DateFormatUtil.getDateFromDateTime(endDate);
			log.info("5. saveDailyEventSchedule () ===> [sDate] >> "+sDate +"[eDate] >>"+eDate + "[occurrencesCount] >> "+occurrencesCount+"[startDate] >> "+startDate);
			/*If start date is greater than given end date(limit) OR Exceed given Occurrences */
			if (sDate.compareTo(eDate)>0 || (occurrencesCount == occurrences)) {
				/* If next record found when update = true, change status as removed before break(event reaches the given End date OR Occurrences) */
				if (scheduleId != 0)
					updateScheduleStatus(scheduleId, event.getId(), status, event.getUpdatedBy());
				break;
			}
		}
		return event;
	}

	/**
	 * Save Weekly based Event
	 */
	private void saveWeeklyEvent(Event event, boolean isUpdate, String status) {
		String repeatOn = "";
		long interval = ConstantUtil.WEEK_IN_SECONDS * event.getSchedule().getRepeatEvery();
		Date initStartDate = event.getSchedule().getRepeatEventStart();
		long taskDuration = interval * event.getSchedule().getOccurrences();
		log.info("1. saveWeeklyEvent () ===> [event] >>"+event+"[interval] >>"+interval+"[initStartDate] >>"+initStartDate+"[taskDuration] >>"+taskDuration);
		if (event.getSchedule().getOccurrences() == 0) 
			event.getSchedule().setEventEnd(event.getSchedule().getRepeatEventEnd());
		/* Find day of the week [Days will start from 1 to 7]*/
		int dayOfTheWeek = DateFormatUtil.getDayOfTheWeek(initStartDate);
		log.info("2. saveWeeklyEvent () ===> [dayOfTheWeek] >> "+dayOfTheWeek);
		int dayDiff = 0;
		Map<Integer, Date> map = new HashMap<Integer, Date>();
		/* Same Task may repeat on multiple times like (One Task may repeat on every 2 weeks once on SUN, MON ,... until task end date) */
		for (Integer selectedDay : event.getSchedule().getRepeatOnList()) {
			if (map.size() != 0)
				repeatOn = repeatOn + ",";
			repeatOn = repeatOn + Integer.toString(selectedDay);
			if (selectedDay > dayOfTheWeek) {
				dayDiff = selectedDay - dayOfTheWeek;
			} else if (selectedDay < dayOfTheWeek) {
				dayDiff = ConstantUtil.DAYS - (dayOfTheWeek - selectedDay);
			} else {
				dayDiff = 0;
			}
			log.info("3. saveWeeklyEvent () ===> [repeatOn] >> "+repeatOn + "[selectedDay] >> "+selectedDay+"[dayDiff] >>"+dayDiff);
			/* Find event StartDate using day of the week & initStartDate */
			event.getSchedule().setRepeatEventStart(DateFormatUtil.addDay(initStartDate, dayDiff));
			if (event.getSchedule().getOccurrences() > 0) {
				event.getSchedule().setRepeatEventEnd(DateFormatUtil.addSeconds(initStartDate, taskDuration));
			}
			map.put(selectedDay, event.getSchedule().getRepeatEventStart());
		}
		event.getSchedule().setRepeatOn(repeatOn);
		List<Map.Entry<Integer, Date>> list = new LinkedList<Map.Entry<Integer, Date>>(map.entrySet());
		map = getMapBySort(list);
		saveEventSchedule(event, event.getSchedule().getRepeatEventStart(), event.getSchedule().getRepeatEventEnd(), map, event.getSchedule().getOccurrences(), isUpdate, status,interval);
	}

	private Map<Integer, Date> getMapBySort(List<Entry<Integer, Date>> list) {
		Collections.sort(list, new Comparator<Map.Entry<Integer, Date>>() {
			@Override
			public int compare(Map.Entry<Integer, Date> o1,
					Map.Entry<Integer, Date> o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
		});
		Map<Integer, Date> map = new LinkedHashMap<Integer, Date>();
		for (Map.Entry<Integer, Date> e : list) {
			map.put(e.getKey(), e.getValue());
		}
		return map;
	}

	/**
	 * Save Weekly based Event schedule
	 */
	private Event saveEventSchedule(Event event, Date startDate, Date endDate, Map<Integer, Date> taskMap, int occurrences, boolean isUpdate, String status, long interval) {
		Map<Integer, Date> mapTask = new HashMap<Integer, Date>();
		mapTask = taskMap;
		int occurrencesCount = 0;
		long taskDuration = 0;
		int scheduleId = 0;
		boolean eventUpdate = true;
		int eventSchduleId = 0;
		boolean isCommentAndFollowerSave = false;
		log.info("1. saveEventSchedule ===> [event] >>"+event +"[startDate] >>"+startDate+"[endDate] >> "+endDate + "[occurrences] >>"+occurrences+ "[isUpdate] >> "+isUpdate + "[status] >> "+status+"[interval] >>"+interval);
		if (isUpdate) {
			scheduleId = event.getSchedule().getId();
			/* Get Before event schedule record and update end date */
			EventSchedule e = scheduleMapper.getBeforeRecordWithAsc(event.getSchedule().getId(), event.getId(), (byte) 0, ConstantUtil.CREATED);
			log.info("1. saveEventSchedule ===> [Before event schedule] >> "+e);
			if (e != null)
				eventSchduleId = e.getId();
		}
		while (true) {
			for (Map.Entry<Integer, Date> map : mapTask.entrySet()) {
				/* Find event occurrence date using event start date */
				startDate = DateFormatUtil.addSeconds(map.getValue(),taskDuration);
				log.info("2. saveEventSchedule ===> [startDate] >> "+startDate + "[Key] >> "+map.getKey());
				if (occurrences == 0) {
					occurrencesCount = 1;
				}
				mapTask.put(map.getKey(), startDate);
				event.getSchedule().setRepeatEventStart(startDate);
				event = addTime(event, true);
				/* Update event start date only once, if find first event occurrence date */
				if (eventUpdate) {
					Date start = DateFormatUtil.addTime(startDate, event.getEventStart());
					Event e = eventMapper.selectByPrimaryKey(event.getId());
					long dayDiffInLong = Math.abs(findDayDiff(e.getEventStart(), e.getEventEnd()));
					/* Convert long day to int day */
					Long day = (dayDiffInLong / (1000 * 60 * 60 * 24));
					Date end = DateFormatUtil.addDay(start, day.intValue());
					log.info("3. saveEventSchedule ===> [event] >> "+e + "[DayDiff] >> "+dayDiffInLong + "[day] >> "+day+"[start] >>" +start+"[end] >>"+end);	
					eventMapper.updateEventStartAndEndDate(event.getId(), start, end);
				}
				eventUpdate = false;
				Date sDate = DateFormatUtil.getDateFromDateTime(startDate);
				Date eDate = DateFormatUtil.getDateFromDateTime(endDate);
				log.info("4. saveEventSchedule ===> [sDate] >> "+sDate + "[eDate] >> "+eDate);	
				/*If Start date is greater than Given end date, update the status[REMOVED] for remaining Repeat event */
				if (sDate.compareTo(eDate) > 0) {
					if (scheduleId != 0)
						updateScheduleStatus(scheduleId, event.getId(), status, event.getUpdatedBy());
					break;
				}
				/* Insert the schedule, if there is no next record found when update = true OR Insert the record when update = false */
				log.info("5. saveEventSchedule ===> [scheduleId] >> "+scheduleId + "[isUpdate] >> "+isUpdate + "[Constant_Event_id] >>"+ConstantUtil.EVENT_ID);
				if (scheduleId == 0 || isUpdate == false) {
					int eventId = event.getId();
					if (isUpdate && status.equalsIgnoreCase(ConstantUtil.UPDATE_ALL) || status.equalsIgnoreCase(ConstantUtil.UPDATE_FOLLOWING_EVENT)) {
						if (ConstantUtil.EVENT_ID != 0)
							eventId = ConstantUtil.EVENT_ID;
					}
					event.getSchedule().setEventId(eventId);
					event.getSchedule().setIsSingle(false);
					if(scheduleId == 0 && isUpdate == true)
						event.getSchedule().setCreatedBy(event.getUpdatedBy());
					event.getSchedule().setUpdatedBy(event.getUpdatedBy());
					log.info("6. saveEventSchedule ===> [schedule] >> "+event.getSchedule());
					scheduleMapper.insert(event.getSchedule());
					if (!isUpdate)
						saveEventCommentAndFollower(event, isUpdate);
				}
				log.info("7. saveEventSchedule ===> [scheduleId] >> "+scheduleId+"[isUpdate] >> "+isUpdate);
				if (isUpdate && scheduleId != 0) {
					event.getSchedule().setId(scheduleId);
					scheduleId = updateEventSchedule(event.getSchedule(), status, eventSchduleId, event.getUpdatedBy());
					eventSchduleId = 0;
					if(!isCommentAndFollowerSave)
						saveEventCommentAndFollower(event, isUpdate);
					isCommentAndFollowerSave = false;
				}
				occurrencesCount++;
				/*If Start date is greater than Given Occurrence count , update the status[REMOVED] for remaining Repeat event if any repeat event found */
				log.info("8. saveEventSchedule ===> [occurrencesCount] >> "+occurrencesCount+"[occurrences] >> "+occurrences);
				if (occurrencesCount == occurrences) {
					if (scheduleId != 0)
						updateScheduleStatus(scheduleId, event.getId(), status, event.getUpdatedBy());
					break;
				}
			}
			taskDuration = interval;
			Date sDate = DateFormatUtil.getDateFromDateTime(startDate);
			Date eDate = DateFormatUtil.getDateFromDateTime(endDate);
			log.info("9. saveEventSchedule ===> [taskDuration] >> "+taskDuration+"[sDate] >> "+sDate +"[eDate] >>"+eDate);
			if (sDate.compareTo(eDate)>0 || (occurrencesCount == occurrences)) {
				break;
			}
		}
		return event;
	}

	/**
	 * Save Monthly based Event
	 */
	private void saveMonthlyEvent(Event event, boolean isUpdate, String status) {
		/* Since month is having irregular interval like (30, 31, 28, 29 days), we have handled it in diff way. */
		if (event.getSchedule().getOccurrences() == 0) 
			event.getSchedule().setEventEnd(event.getSchedule().getRepeatEventEnd());
		/* Find event end date, If user entered Occurrence instead of End date */
		if (event.getSchedule().getOccurrences() > 0) {
			int monthCount = event.getSchedule().getOccurrences()* event.getSchedule().getRepeatEvery();
			Date endDate = DateFormatUtil.addMonth(event.getSchedule().getRepeatEventStart(), monthCount);
			event.getSchedule().setRepeatEventEnd(endDate);
		}
		log.info("1. saveMonthlyEvent () ===> [event] >> "+event+ "[EventStart] >> "+event.getSchedule().getRepeatEventStart() +"[endDate] >>"+event.getSchedule().getRepeatEventEnd()+"[Occurrences] >>"+event.getSchedule().getOccurrences()+"[isUpdate] >>"+isUpdate+"[status] >>"+status);
		saveMonthlyEventSchedule(event, event.getSchedule().getRepeatEventStart(), event.getSchedule().getRepeatEventEnd(), event.getSchedule().getOccurrences(), isUpdate, status);
	}

	/**
	 * Save Monthly based Event schedule
	 */
	private void saveMonthlyEventSchedule(Event event, Date startDate, Date endDate, int occurrence, boolean isUpdate, String status) {
		int eventStartDay = DateFormatUtil.getDayOfTheMonth(startDate);
		int repeatMonth = event.getSchedule().getRepeatEvery();
		int occurrenceCount = 0;
		int eventRepeatDay = DateFormatUtil.getDayOfTheMonth(startDate);
		int scheduleId = 0;
		int eventSchduleId = 0;
		boolean isCommentAndFollowerSave = false;
		if (isUpdate) {
			scheduleId = event.getSchedule().getId();
			/* Get Before event schedule record and update end date */
			EventSchedule e = scheduleMapper.getBeforeRecordWithAsc(event.getSchedule().getId(), event.getId(), (byte) 0, ConstantUtil.CREATED);
			log.info("1. saveMonthlyEventSchedule () ===> [Before event schedule] >>"+e);
			if (e != null)
				eventSchduleId = e.getId();
		}
		while (true) {
			event.getSchedule().setRepeatEventStart(startDate);
			event = addTime(event, true);
			Date tempDate = event.getSchedule().getRepeatEventStart();
			if (occurrence == 0) {
				occurrenceCount = 1;
			}
			Date sDate = DateFormatUtil.getDateFromDateTime(startDate);
			Date eDate = DateFormatUtil.getDateFromDateTime(endDate);
			log.info("2. saveMonthlyEventSchedule () ===> [sDate] >>"+sDate+"[eDate] >>"+endDate+"[occurrence] >>"+occurrence + "[scheduleId] >> "+scheduleId);
			if (occurrence == 0 && sDate.compareTo(eDate) > 0) {
				/*  Update status = removed for remaining event, when start date is greater than event end date */
				if (scheduleId != 0)
					scheduleMapper.updateScheduleStatus(scheduleId, event.getId(), event.getUpdatedBy(), ConstantUtil.REMOVED);
				break;
			}
			/*  Insert the schedule, if there is no next record found when update = true */
			log.info("3. saveMonthlyEventSchedule () ===> [scheduleId] >>"+scheduleId+"[isUpdate] >>"+isUpdate);
			if ((scheduleId == 0 && isUpdate)) {
				occurrenceCount++;
				int eventId = event.getId();
				if (ConstantUtil.EVENT_ID != 0)
					eventId = ConstantUtil.EVENT_ID;
				event.getSchedule().setEventId(eventId);
				event.getSchedule().setIsSingle(ConstantUtil.IS_SINGLE);
				event.getSchedule().setRepeatOn("");
				if(scheduleId == 0 && isUpdate == true)
					event.getSchedule().setCreatedBy(event.getUpdatedBy());
				event.getSchedule().setUpdatedBy(event.getUpdatedBy());
				log.info("4. saveMonthlyEventSchedule () ===> [schedule] >>"+event.getSchedule());
				scheduleMapper.insert(event.getSchedule());
			}
			log.info("5. saveMonthlyEventSchedule () ===> [occurrenceCount] >>"+occurrenceCount + "[occurrence] >> "+ occurrence + "[isUpdate] >> "+isUpdate);
			if ((occurrenceCount == occurrence && isUpdate)) {
				break;
			}
			/*This will execute when isUpdate = true*/
			if (isUpdate && scheduleId != 0) {
				event.getSchedule().setId(scheduleId);
				if (occurrence != 0)
					occurrenceCount++;
				event.getSchedule().setRepeatOn("");
				scheduleId = updateEventSchedule(event.getSchedule(), status, eventSchduleId, event.getUpdatedBy());
				eventSchduleId = 0;
				if(!isCommentAndFollowerSave)
					saveEventCommentAndFollower(event, isUpdate);
				isCommentAndFollowerSave = false;
			}
			/* Insert the record when update = false */
			log.info("6. saveMonthlyEventSchedule () ===> [eventStartDay] >>"+eventStartDay + "[eventRepeatDay] >> "+ eventRepeatDay + "[isUpdate] >> "+isUpdate+"[occurrenceCount] >> "+occurrenceCount);
			if ((eventStartDay == eventRepeatDay && !isUpdate)) {
				event.getSchedule().setRepeatOn("");
				scheduleMapper.insert(event.getSchedule());
				occurrenceCount++;
				if (!isUpdate)
					saveEventCommentAndFollower(event, isUpdate);
			}
			startDate = DateFormatUtil.addMonth(startDate, repeatMonth);
			tempDate = startDate;
			eventRepeatDay = DateFormatUtil.getDayOfTheMonth(startDate);
			log.info("7. saveMonthlyEventSchedule () ===> [startDate] >>"+startDate + "[tempDate] >> "+ eventRepeatDay+"[eventRepeatDay] >>"+eventRepeatDay);
			if (eventStartDay != eventRepeatDay) {
				int dayDiff = Math.abs(eventStartDay - eventRepeatDay);
				startDate = DateFormatUtil.addDay(startDate, dayDiff);
				eventRepeatDay = DateFormatUtil.getDayOfTheMonth(startDate);
				if (eventStartDay != eventRepeatDay)
					startDate = tempDate;
			}
			log.info("7. saveMonthlyEventSchedule () ===> [occurrence] >>"+occurrence + "[occurrenceCount] >> "+ occurrenceCount);
			if (occurrence == occurrenceCount) {
				/*  if there is a next record found when update = true change status as removed before break (event reaches the end date) */
				if (scheduleId != 0)
					updateScheduleStatus(scheduleId, event.getId(), status,event.getUpdatedBy());
				break;
			}
		}
	}

	/**
	 * Save Yearly based Event
	 */
	private void saveYearlyEvent(Event event, boolean isUpdate, String status) {
		long interval = 0;
		Calendar c = Calendar.getInstance();
		c.setTime(event.getSchedule().getRepeatEventStart());
		int year = c.get(Calendar.YEAR);
		int month = 1 + c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);
		if (event.getSchedule().getOccurrences() == 0)
			event.getSchedule().setEventEnd(
					event.getSchedule().getRepeatEventEnd());
		/*
		 * Check leap year
		 */
		boolean isLeapYear = isLeapYear(year);
		boolean isFeb29 = false;
		/*
		 * Check given year is leap year or normal year. If leap year (feb 29),
		 * next event occurrence date is after 4 years.
		 */
		if (event.getSchedule().getOccurrences() != 0) {
			if (isLeapYear && (day == 29) && (month == 2)) {
				interval = ConstantUtil.LEAP_YEAR_IN_SECONDS
						* (event.getSchedule().getRepeatEvery() * 4);
				isFeb29 = true;
			} else {
				interval = ConstantUtil.NORMAL_YEAR_IN_SECONDS
						* event.getSchedule().getRepeatEvery();
			}
			long eventOccurrence = interval
					* ((event.getSchedule().getOccurrences()));
			Date endDate = DateFormatUtil.addSeconds(event.getSchedule()
					.getRepeatEventStart(), eventOccurrence);
			event.getSchedule().setRepeatEventEnd(endDate);
		}
		saveYearlyEventSchedule(event, event.getSchedule()
				.getRepeatEventStart(),
				event.getSchedule().getRepeatEventEnd(), isFeb29, event
						.getSchedule().getOccurrences(), isUpdate, status);
	}

	/**
	 * Save Yearly based event schedule
	 */
	private void saveYearlyEventSchedule(Event event, Date startDate,
			Date endDate, boolean isFeb29, int occurrences, boolean isUpdate,
			String status) {
		int occurrencesCount = 0;
		int scheduleId = 0;
		int eventSchduleId = 0;
		if (isUpdate) {
			scheduleId = event.getSchedule().getId();
			/*
			 * Get Before event schedule record and update end date
			 */
			EventSchedule e = scheduleMapper.getBeforeRecordWithAsc(event.getSchedule().getId(), event.getId(), (byte) 0, ConstantUtil.CREATED);
			if (e != null)
				eventSchduleId = e.getId();
		}
		while (true) {

			event.getSchedule().setRepeatEventStart(startDate);
			event = addTime(event, true);
			if (scheduleId == 0 || isUpdate == false) {
				scheduleMapper.insert(event.getSchedule());
				saveEventCommentAndFollower(event, isUpdate);
			}

			if (isUpdate && scheduleId != 0) {
				event.getSchedule().setId(scheduleId);
				scheduleId = updateEventSchedule(event.getSchedule(), status,eventSchduleId, event.getUpdatedBy());
				eventSchduleId = 0;
			}

			if (isFeb29) {
				startDate = DateFormatUtil.addYear(startDate,
						(ConstantUtil.LEAP_YEAR * event.getSchedule()
								.getRepeatEvery()));
			} else {
				startDate = DateFormatUtil.addYear(startDate, event
						.getSchedule().getRepeatEvery());
			}
			occurrencesCount++;
			if ((occurrences == occurrencesCount)
					|| (startDate.getTime() > endDate.getTime())) {
				/*
				 * if there is a next record found when update = true change
				 * status as removed before break (event reaches the end date)
				 */
				if (scheduleId != 0)
					updateScheduleStatus(scheduleId, event.getId(), status,
							event.getUpdatedBy());
				break;
			}
		}
	}

	/**
	 * Find leap year
	 */
	private Boolean isLeapYear(int year) {
		boolean isLeapYear = false;
		if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
			isLeapYear = true;
		}
		return isLeapYear;
	}

	/**
	 * Save Comment and follower for task OR Event
	 */
	private void saveEventCommentAndFollower(Event event, boolean isUpdate) {
		if (event.getComments() != null) {
			for (EventComment comment : event.getComments()) {
				comment.setEventScheduleId(event.getSchedule().getId());
				comment.setEventId(event.getSchedule().getEventId());
				comment.setUpdatedBy(event.getUpdatedBy());
				comment.setIsActive(true);
				if(isUpdate){
					log.info("1. saveEventCommentAndFollower () ===> Event Comment [UPDATE or INSERT] >> "+comment);
					if(comment.getId() != 0)
						commentMapper.updateByPrimaryKey(comment);
					if (comment.getId() == 0){
						comment.setCreatedBy(event.getCreatedBy());
						commentMapper.insert(comment);
					}
				}else if(!isUpdate){
				comment.setCreatedBy(event.getCreatedBy());
				log.info("2. saveEventCommentAndFollower () ===> Event Comment [INSERT] >> "+comment);
				commentMapper.insert(comment);
				}
			}
		}
		if (event.getFollowers() != null) {
			for (EventFollower follower : event.getFollowers()) {
				EventFollower followerFromDb = followerMapper.selectFollower(event.getSchedule().getId(), event.getSchedule().getEventId(), follower.getFollowerId());
				follower.setEventScheduleId(event.getSchedule().getId());
				follower.setEventId(event.getSchedule().getEventId());
				follower.setUpdatedBy(event.getUpdatedBy());
				follower.setIsActive(true);
				if (isUpdate) {
					log.info("3. saveEventCommentAndFollower () ===> Event Follower [UPDATE or INSERT]>> "+follower);
					if (follower.getId() != 0)
						followerMapper.updateByPrimaryKey(follower);
					if (follower.getId() == 0){
						follower.setCreatedBy(event.getCreatedBy());
						if(followerFromDb == null)
						followerMapper.insert(follower);
					}
				} else if (!isUpdate){
					log.info("4. saveEventCommentAndFollower () ===> Event Follower [INSERT]>> "+follower);
					follower.setCreatedBy(event.getCreatedBy());
					if(followerFromDb == null){
					followerMapper.insert(follower);
					}
				}
			}
		}

	}


	/**
	 * Get Event schedule
	 */
	private EventScheduleJSON getTaskSchedule(EventSchedule schedule,
			Date eventStart) {
		EventScheduleJSON json = new EventScheduleJSON();
		if (schedule != null) {
			json.setId(schedule.getId());
			json.setEventId(schedule.getEventId());
			json.setTitle(schedule.getTitle());
			json.setDescription(schedule.getDescription());
			json.setRepeatEventEnd(schedule.getRepeatEventEnd());
			json.setRepeatEventStart(schedule.getRepeatEventStart());
			json.setProjectName(CUtil.CheckNull(schedule.getProjectName()));
			json.setRepeatEventStart(schedule.getRepeatEventStart());
			json.setRepeatEventEnd(schedule.getRepeatEventEnd());
			json.setComments(CUtil.CheckNull(schedule.getComments()));
			json.setStatus(schedule.getStatus());
			json.setEventStart(eventStart);
			json.setEventEnd(schedule.getEventEnd());
			if (schedule.getRepeatType() != null)
				json.setRepeatType(schedule.getRepeatType());
			json.setCommentList(schedule.getEventComments());
			if (schedule.getEvent() != null) {
				json.setEvent(getEvent(schedule.getEvent()));
			}
			if (schedule.getFollowerList() != null) {
				json.setFollowerList(followerList(schedule.getFollowerList()));
			}
		}
		return json;
	}

	/**
	 * Get Event
	 */
	private EventJSON getEvent(Event event) {
		EventJSON json = new EventJSON();
		if (event != null) {
			json.setAssignedFromId(event.getAssignedFromId());
			json.setId(event.getId());
			if (event.getAssignedFrom() != null) {
				json.setAssignedFromName(event.getAssignedFrom().getName());
			}
			json.setAssignedFromId(event.getAssignedFromId());
			json.setAssignedType(event.getAssignedType());
			json.setRepeatEvent(event.getIsRepeatEvent());
			json.setEventStart(event.getEventStart());
			json.setSourceName(event.getSourceName());
			json.setEventEnd(event.getEventEnd());
			json.setRepeatEvent(event.getIsRepeatEvent());
			if(event.getIsAllDay() != null)
				json.setAllDay(event.getIsAllDay());
			else
				json.setAllDay(false);
			json.setTypeId(event.getType());
			if (event.getType() == ConstantUtil.EVENT_TYPE)
				json.setType(ConstantUtil.EVENT);
			else
				json.setType(ConstantUtil.TASK);
			/*This will execute when task & No Repeat event for an Event*/
			if(!event.getIsRepeatEvent()){
				if(event.getScheduleList() != null && event.getScheduleList().size() >0){
				for(EventSchedule s : event.getScheduleList()){
				EventScheduleJSON schedule = new EventScheduleJSON();
				schedule.setId(s.getId());
				schedule.setCommentList(s.getEventComments());
				schedule.setFollowerList(followerList(s.getFollowerList()));
				schedule.setProjectName(s.getProjectName());
				schedule.setRepeatEventStart(s.getRepeatEventStart());
				schedule.setRepeatEventEnd(s.getRepeatEventEnd());
				schedule.setOccurrences(s.getOccurrences());
				schedule.setEventEnd(s.getEventEnd());
				schedule.setDescription(s.getDescription());
				schedule.setTitle(s.getTitle());
				schedule.setEventId(s.getEventId());
				schedule.setStatus(s.getStatus());
				schedule.setComments(s.getComments());
				schedule.setIs_single(s.getIsSingle());
				schedule.setRepeatType(s.getRepeatType());
				schedule.setRepeatOn(s.getRepeatOn());
				json.setSchedule(schedule);
			}
			}
			}
		}
		return json;
	}

	/**
	 * Get follower list for particular Event schedule
	 */

	private List<EventFollower> followerList(List<EventFollower> list) {
		List<EventFollower> followers = new ArrayList<EventFollower>();
		for (EventFollower follower : list) {
			EventFollower followerObj = new EventFollower();
			followerObj.setId(follower.getId());
			followerObj.setFollowerId(follower.getFollowerId());
			UserInfo u = new UserInfo();
			if (follower.getFollower() != null) {
				u.setName(follower.getFollower().getName());
				followerObj.setFollowerName(u.getName());
			}
			followerObj.setEventScheduleId(follower.getEventScheduleId());
			followerObj.setEventId(follower.getEventId());
			followers.add(followerObj);
		}
		return followers;
	}

	@Override
	public List<Event> listAll(int userId, int eventId, String startDate, String endDate, String status, String sourceName, String type, int begin, int end) {
		List<Event> list = null;
		if(!startDate.isEmpty() && endDate.isEmpty()){
			endDate = startDate;
		}
		if (begin == 0 && end == 0) {
			list = eventMapper.findByassignedFromId(userId, eventId, startDate, endDate, status, sourceName, type);
		} else {
			list = eventMapper.findByassignedFromIdWithLimit(userId, eventId, startDate, endDate, status, sourceName, type, new EventFollowerExample(), new RowBounds(begin, end));
		}
		return list;
	}


	@Override
	public int updateEventStatus(String status, int eventScheduleId, int updatedBy) {
		EventSchedule schedule = scheduleMapper.selectByPrimaryKey(eventScheduleId);
		log.info("updateEventStatus () ===> [schedule] >>"+schedule+"[status] >> "+status+"[eventScheduleId] >>"+eventScheduleId);
		int updateCount = 0;
		if(schedule != null){
		if (status.equalsIgnoreCase(ConstantUtil.DELETE_ALL)) {
			updateCount = scheduleMapper.updateScheduleByEventId(schedule.getEventId(),updatedBy, ConstantUtil.REMOVED, ConstantUtil.DELETE_ALL_COMMENT);
			updateCount = followerMapper.deleteByEventId(schedule.getEventId(), ConstantUtil.IS_ACTIVE, updatedBy);
			updateCount = commentMapper.deleteByEventId(schedule.getEventId(), ConstantUtil.IS_ACTIVE, updatedBy);
			updateCount = eventMapper.deleteEventById(schedule.getEventId(), ConstantUtil.IS_ACTIVE, updatedBy);
		} else if (status.equalsIgnoreCase(ConstantUtil.DELETE_SINGLE)) {
			updateCount = scheduleMapper.updateScheduleByScheduleId(eventScheduleId, updatedBy, ConstantUtil.REMOVED, ConstantUtil.DELETE_SINGLE_COMMENT);
			updateCount = followerMapper.deleteByScheduleId(schedule.getId(), (byte) ConstantUtil.IS_ACTIVE, updatedBy);
			updateCount = commentMapper.deleteByScheduleId(schedule.getId(), (byte) ConstantUtil.IS_ACTIVE, updatedBy);
		} else if (status.equalsIgnoreCase(ConstantUtil.DELETE_FOLLOWING_EVENT)) {
			updateCount = scheduleMapper.updateScheduleByStartDate(schedule.getEventId(), schedule.getRepeatEventStart(), updatedBy,
					ConstantUtil.REMOVED, ConstantUtil.DELETE_FOLLOWING_EVENT_COMMENT);
			updateCount = followerMapper.deleteByFollowingScheduleId(schedule.getEventId(), schedule.getId(), (byte) ConstantUtil.IS_ACTIVE, updatedBy);
			updateCount = commentMapper.deleteByFollowingScheduleId(schedule.getEventId(), schedule.getId(), (byte) ConstantUtil.IS_ACTIVE, updatedBy);
		} else if (status.equalsIgnoreCase(ConstantUtil.COMPLETED) || status.equalsIgnoreCase(ConstantUtil.REMOVED)) {
				String comment = "";
				if(status.equalsIgnoreCase(ConstantUtil.COMPLETED))
					comment = ConstantUtil.COMPLETED_COMMENT;
				else
					comment = ConstantUtil.REMOVED;
				updateCount = scheduleMapper.updateScheduleByEventId(schedule.getEventId(), updatedBy, status, comment);
			}
		
		}
		log.info("updateEventStatus () [updateCount] >>"+updateCount);
		return updateCount;
	}

	private int updateEventSchedule(EventSchedule schedule, String status, int scheduleId, int updatedBy) {
		log.info("1. updateEventSchedule () ===> [schedule] >> "+schedule+"[status] >>"+status + "[scheduleId] >> "+scheduleId);
		int id = 0;
		schedule.setStatus(ConstantUtil.CREATED);
		/* Update all event schedule for given event id.*/
		if (status.equalsIgnoreCase(ConstantUtil.UPDATE_ALL)|| status.isEmpty()) {
			schedule.setIsSingle(false);
			/*Update before record also*/
			 if (scheduleId != 0) {
				schedule.setId(scheduleId);
			}
			if (schedule.getIsSingle()) {
				schedule.setOccurrences(0);
				schedule.setEventEnd(schedule.getRepeatEventEnd());
				schedule.setUpdatedBy(updatedBy);
				scheduleMapper.updateEventScheduleAll(schedule);
			} else{
				schedule.setUpdatedBy(updatedBy);
				scheduleMapper.updateAll(schedule);
			}
		} else if (status.equalsIgnoreCase(ConstantUtil.UPDATE_FOLLOWING_EVENT)) {
			/* Get Before event schedule record and update end date */
			if (ConstantUtil.EVENT_ID != 0) {
				EventSchedule e = scheduleMapper.getBeforeRecord(schedule.getId(), schedule.getEventId(), (byte) 0);
				if (e != null) {
					Date date = DateFormatUtil.getDateFromDateTime(e.getRepeatEventEnd());
					/* set isSingle = false because we have to update except single event */
					log.info("2. updateEventSchedule () ===> [date] >> "+date +"[scheduleId] >> "+schedule.getId()+ "[eventId] >>"+e.getEventId());
					scheduleMapper.updateEndDate(date, schedule.getId(), e.getEventId(), (byte) 0, 0);
				}
			}
			/* Update following event and eventId */
			schedule.setUpdatedBy(updatedBy);
			scheduleMapper.updateFollowingEvent(schedule);
			int eventScheduleId = schedule.getEventId();
			log.info("3. updateEventSchedule () ===> [CONSTANT_EVENT_ID] >> "+ConstantUtil.EVENT_ID);
			if (ConstantUtil.EVENT_ID != 0)
				eventScheduleId = ConstantUtil.EVENT_ID;
			log.info("4. updateEventSchedule () ===> [schedule] >> "+schedule + "[eventScheduleId] >>"+eventScheduleId);
			scheduleMapper.updateEventId(schedule.getId(),schedule.getRepeatEventStart(), eventScheduleId);
		} else if (status.equalsIgnoreCase(ConstantUtil.UPDATE_SINGLE)) {
			schedule.setIsSingle(true);
			schedule.setOccurrences(0);
			schedule.setEventEnd(schedule.getRepeatEventEnd());
			schedule.setUpdatedBy(updatedBy);
			log.info("5. updateEventSchedule () ===> [schedule] >> "+schedule);
			scheduleMapper.updateSingleEvent(schedule);
		}
		EventSchedule s = scheduleMapper.selectEventSchedule(schedule.getId(),schedule.getEventId(), ConstantUtil.CREATED);
		if (s != null)
			id = s.getId();
		log.info("6. updateEventSchedule () ===> [id] >> "+id);
		return id;
	}

	/**
	 * Update remaining event schedule. While updating the event User may give
	 * less end date ( which is smaller than previous end date (Inserted end
	 * date in first time) ).
	 */
	private void updateScheduleStatus(int scheduleId, int eventId, String status, int updatedBy) {
		log.info("updateScheduleStatus () ==> scheduleId >> "+scheduleId+ "eventId >> "+eventId + "status >> "+status);
		if (status.equalsIgnoreCase(ConstantUtil.UPDATE_FOLLOWING_EVENT)) {
			scheduleMapper.updateFollowingScheduleStatus(scheduleId, eventId, ConstantUtil.REMOVED);
			followerMapper.deleteByFollowingScheduleId(eventId, scheduleId, ConstantUtil.IS_ACTIVE, updatedBy);
			commentMapper.deleteByFollowingScheduleId(eventId, scheduleId, ConstantUtil.IS_ACTIVE, updatedBy);
		} else {
			scheduleMapper.updateScheduleStatus(scheduleId, eventId, updatedBy, ConstantUtil.REMOVED);
		}
	}

	@Override
	public int addComment(EventComment comment) {
		return commentMapper.insert(comment);
	}

	@Override
	public EventJSON findByScheduleId(int scheduleId) {
		EventSchedule eventSchedule = scheduleMapper .selectByScheduleId(scheduleId);
		Event e = null;
		Date eventStart = eventSchedule.getEvent().getEventStart();
		EventJSON json = getEvent(eventSchedule.getEvent());
		/**
		 * Make schedule list if isRepeatEvent = true
		 */
		if (json.isRepeatEvent())
			eventSchedule.setEvent(e);
		json.setSchedule(getTaskSchedule(eventSchedule, eventStart));
		return json;
	}

}
