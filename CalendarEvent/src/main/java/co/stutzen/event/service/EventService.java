package co.stutzen.event.service;

import java.util.List;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.event.entity.Event;
import co.stutzen.event.entity.EventComment;
import co.stutzen.event.entity.EventJSON;

public interface EventService extends SWFService<Event> {

	void saveEventSchedule(Event event,boolean isUpdate);

	int updateEventStatus(String status, int eventScheduleId, int modifiedBy);

	int addComment(EventComment comment);
	
	List<Event> listAll(int userId, int eventId, String startDate, String endDate, String status, String sourceName, String type, int begin, int end);

	void updateEvent(Event event, String status);

	EventJSON findByScheduleId(int scheduleId);


}
