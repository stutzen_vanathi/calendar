package co.stutzen.event.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is created to make proper event schedule bean for Front end
 *
 */
public class EventScheduleJSON {
	private int id;
	private int eventId;
	private Date repeatEventStart;
	private Date repeatEventEnd;
	private String repeatType;
	private String title;
	private String description;
	private String projectName;
	private int occurrences;
	private Date eventEnd;
	private Date eventStart;
	private boolean is_single;
	private String status;
	private String comments;
	private String repeatOn;
	private EventJSON event;
	private List<EventComment> commentList = new ArrayList<EventComment>();
	private List<EventFollower> followerList = new ArrayList<EventFollower>();
	
	public String getRepeatOn() {
		return repeatOn;
	}
	
	public void setRepeatOn(String repeatOn) {
		this.repeatOn = repeatOn;
	}
	public int getId() {
		return id;
	}
	public int getEventId() {
		return eventId;
	}
	public Date getRepeatEventStart() {
		return repeatEventStart;
	}
	public Date getRepeatEventEnd() {
		return repeatEventEnd;
	}
	public String getRepeatType() {
		return repeatType;
	}
	public String getTitle() {
		return title;
	}
	public String getDescription() {
		return description;
	}
	public String getProjectName() {
		return projectName;
	}
	public int getOccurrences() {
		return occurrences;
	}
	public Date getEventEnd() {
		return eventEnd;
	}
	public Date getEventStart() {
		return eventStart;
	}
	public boolean isIs_single() {
		return is_single;
	}
	public String getStatus() {
		return status;
	}
	public String getComments() {
		return comments;
	}
	public EventJSON getEvent() {
		return event;
	}
	public List<EventComment> getCommentList() {
		return commentList;
	}
	public List<EventFollower> getFollowerList() {
		return followerList;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	public void setRepeatEventStart(Date repeatEventStart) {
		this.repeatEventStart = repeatEventStart;
	}
	public void setRepeatEventEnd(Date repeatEventEnd) {
		this.repeatEventEnd = repeatEventEnd;
	}
	public void setRepeatType(String repeatType) {
		this.repeatType = repeatType;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public void setOccurrences(int occurrences) {
		this.occurrences = occurrences;
	}
	public void setEventEnd(Date eventEnd) {
		this.eventEnd = eventEnd;
	}
	public void setEventStart(Date eventStart) {
		this.eventStart = eventStart;
	}
	public void setIs_single(boolean is_single) {
		this.is_single = is_single;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public void setEvent(EventJSON event) {
		this.event = event;
	}
	public void setCommentList(List<EventComment> commentList) {
		this.commentList = commentList;
	}
	public void setFollowerList(List<EventFollower> followerList) {
		this.followerList = followerList;
	}	
}
