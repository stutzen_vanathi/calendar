package co.stutzen.event.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is created to make proper event bean for Front end
 * 
 */
public class EventJSON {
	private int id;
	private String assignedFromName;
	private int assignedFromId;
	private int assignedToId;
	private String assignedToName;
	private String assignedType;
	/*private String title;
	private String description;*/
	private Date eventStart;
	private Date eventEnd;
	private boolean isRepeatEvent;
	private String type;
	private int typeId;
	private String sourceName;
	private boolean isAllDay;
	private List<Event> events = new ArrayList<Event>();
	private List<EventScheduleJSON> schedules = new ArrayList<EventScheduleJSON>();
	private EventScheduleJSON schedule;

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public boolean isAllDay() {
		return isAllDay;
	}

	public void setAllDay(boolean isAllDay) {
		this.isAllDay = isAllDay;
	}

	public String getSourceName() {
		return sourceName;
	}
	
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	public EventScheduleJSON getSchedule() {
		return schedule;
	}
	public void setSchedule(EventScheduleJSON schedule) {
		this.schedule = schedule;
	}
	public int getId() {
		return id;
	}
	public String getAssignedFromName() {
		return assignedFromName;
	}
	public int getAssignedFromId() {
		return assignedFromId;
	}
	public int getAssignedToId() {
		return assignedToId;
	}
	public String getAssignedToName() {
		return assignedToName;
	}
	public String getAssignedType() {
		return assignedType;
	}
	public Date getEventStart() {
		return eventStart;
	}
	public Date getEventEnd() {
		return eventEnd;
	}
	public boolean isRepeatEvent() {
		return isRepeatEvent;
	}
	public String getType() {
		return type;
	}
	public List<Event> getEvents() {
		return events;
	}
	public List<EventScheduleJSON> getSchedules() {
		return schedules;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setAssignedFromName(String assignedFromName) {
		this.assignedFromName = assignedFromName;
	}
	public void setAssignedFromId(int assignedFromId) {
		this.assignedFromId = assignedFromId;
	}
	public void setAssignedToId(int assignedToId) {
		this.assignedToId = assignedToId;
	}
	public void setAssignedToName(String assignedToName) {
		this.assignedToName = assignedToName;
	}
	public void setAssignedType(String assignedType) {
		this.assignedType = assignedType;
	}
	public void setEventStart(Date eventStart) {
		this.eventStart = eventStart;
	}
	public void setEventEnd(Date eventEnd) {
		this.eventEnd = eventEnd;
	}
	public void setRepeatEvent(boolean isRepeatEvent) {
		this.isRepeatEvent = isRepeatEvent;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setEvents(List<Event> events) {
		this.events = events;
	}
	public void setSchedules(List<EventScheduleJSON> schedules) {
		this.schedules = schedules;
	}	
}
