package co.stutzen.event.entity;

import java.util.List;

/**
 * This class is created to make proper task bean based on follower for Front
 * end
 * 
 */

public class EventFollowerJSON {

	private int followerId;
	private String followerName;
	private List<EventScheduleJSON> schedules;

	public int getFollowerId() {
		return followerId;
	}

	public String getFollowerName() {
		return followerName;
	}

	public List<EventScheduleJSON> getSchedules() {
		return schedules;
	}

	public void setFollowerId(int followerId) {
		this.followerId = followerId;
	}

	public void setFollowerName(String followerName) {
		this.followerName = followerName;
	}

	public void setSchedules(List<EventScheduleJSON> schedules) {
		this.schedules = schedules;
	}

}
